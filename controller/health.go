package controller

import (
	"go-api/service"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type HealthController struct {
	db *gorm.DB
}

func NewHealthController(db *gorm.DB) HealthController {
	return HealthController{db: db}
}
func (hc HealthController) HealthCheck(c *fiber.Ctx) error {
	healthService := service.NewHealthService(hc.db)
	result, err := healthService.CheckHealth()
	if err != nil {
		return c.Status(500).JSON(&fiber.Map{
			"success": false,
			"message": "Internal Server Error",
			"data":    nil,
		})
	}

	return c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "",
		"data":    result,
	})
}
