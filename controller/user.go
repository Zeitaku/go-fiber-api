package controller

import (
	"errors"
	"go-api/model"
	"go-api/service"
	"regexp"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"github.com/sirupsen/logrus"
	"gopkg.in/gomail.v2"
	"gorm.io/gorm"
)

type UserController struct {
	db     *gorm.DB
	Logger *logrus.Logger
	Mailer *gomail.Dialer
}

func NewUserController(db *gorm.DB, logger *logrus.Logger, mailer *gomail.Dialer) UserController {
	return UserController{db: db, Logger: logger, Mailer: mailer}
}

func (uh UserController) GetAllUsers(c *fiber.Ctx) error {
	page := c.Query("page")
	limit := c.Query("limit")
	userService := service.NewUserService(uh.db, uh.Mailer)
	pagination, result, err := userService.GetAllUsers(page, limit)
	if err != nil {
		return c.Status(500).JSON(&fiber.Map{
			"success": false,
			"message": err.Error(),
			"data":    nil,
		})
	}

	return c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "Berhasil memanggil list user",
		"data":    result,
		"meta":    pagination,
	})
}
func (uh UserController) GetUser(c *fiber.Ctx) error {
	userService := service.NewUserService(uh.db, uh.Mailer)
	result, err := userService.GetUser(c.Params("id"))
	if err != nil {
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err.Error(),
			"data":    nil,
		})
		return err
	}

	c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "Berhasil memanggil user",
		"data":    result,
	})
	return nil
}

func (uh UserController) SaveUser(c *fiber.Ctx) error {
	newUser := new(model.User)

	err := c.BodyParser(newUser)
	is_alphanumeric := regexp.MustCompile(`^[a-zA-Z0-9]*$`).MatchString(newUser.Name)
	if len(newUser.Name) > 12 {
		err = errors.New("nama lebih dari 12 karakter")
	} else if !is_alphanumeric {
		err = errors.New("nama hanya terdiri dari huruf alphabet dan angka")
	}
	if err != nil {
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
			"data":    nil,
		})
		return err
	}
	userService := service.NewUserService(uh.db, uh.Mailer)
	result, err := userService.CreateUser(newUser.Name, newUser.Email, newUser.Password)
	if err != nil {
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
			"data":    nil,
		})
		return err
	}

	c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "user berhasil dibuat",
		"data":    result,
	})
	return nil
}

func (uh UserController) UpdateUser(c *fiber.Ctx) error {
	updateUser := new(model.User)
	err := c.BodyParser(updateUser)
	is_alphanumeric := regexp.MustCompile(`^[a-zA-Z0-9]*$`).MatchString(updateUser.Name)
	if len(updateUser.Name) > 12 {
		err = errors.New("nama lebih dari 12 karakter")
	} else if !is_alphanumeric {
		err = errors.New("nama hanya terdiri dari huruf alphabet dan angka")
	}
	if err != nil {
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
			"data":    nil,
		})
		return err
	}
	userService := service.NewUserService(uh.db, uh.Mailer)
	result, err := userService.UpdateUser(updateUser.Name, c.Params("id"))
	if err != nil {
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
			"data":    nil,
		})
		return err
	}

	c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "berhasil update username",
		"data":    result,
	})
	return nil
}

func (uh UserController) UpdateUserPassword(c *fiber.Ctx) error {
	updatePass := new(model.UpdatePass)
	err := c.BodyParser(updatePass)
	if err != nil {
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
			"data":    nil,
		})
		return err
	}
	userService := service.NewUserService(uh.db, uh.Mailer)
	result, err := userService.UpdateUserPassword(updatePass.OldPassword, updatePass.NewPassword, c.Params("id"))
	if err != nil {
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
			"data":    nil,
		})
		return err
	}

	c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "berhasil update password",
		"data":    result,
	})
	return nil
}
func (uh UserController) DeleteUser(c *fiber.Ctx) error {
	userService := service.NewUserService(uh.db, uh.Mailer)
	result, err := userService.DeleteUser(c.Params("id"))
	if err != nil {
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
			"data":    nil,
		})
		return err
	}

	c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "data berhasil dihapus",
		"data":    result,
	})
	return nil
}

func (uh UserController) Login(c *fiber.Ctx) error {
	login := new(model.User)
	err := c.BodyParser(login)
	if err != nil {
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
			"data":    nil,
		})
		return err
	}
	userService := service.NewUserService(uh.db, uh.Mailer)
	result, err := userService.Login(login.Email, login.Password)
	if err != nil {
		c.Status(400).JSON(&fiber.Map{
			"success": false,
			"message": err,
			"data":    nil,
		})
		return err
	}
	c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "berhasil login",
		"data":    result,
	})
	return nil
}

func (uh UserController) UserProfile(c *fiber.Ctx) error {
	user := c.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	id := claims["id"].(float64)
	convID := strconv.Itoa(int(id))

	userService := service.NewUserService(uh.db, uh.Mailer)
	result, err := userService.UserProfile(convID)
	if err != nil {
		return c.Status(500).JSON(&fiber.Map{
			"success": false,
			"message": err.Error(),
			"data":    nil,
		})
	}

	return c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "user profile success",
		"data":    result,
	})
}

func (uh UserController) VerifyEmail(c *fiber.Ctx) error {
	email := c.Query("email")
	userService := service.NewUserService(uh.db, uh.Mailer)
	result, err := userService.VerifyEmail(email)
	if err != nil {
		return c.Status(500).JSON(&fiber.Map{
			"success": false,
			"message": err.Error(),
			"data":    nil,
		})
	}

	return c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "user verified!",
		"data":    result,
	})
}

func (uh UserController) GetVerified(c *fiber.Ctx) error {
	userService := service.NewUserService(uh.db, uh.Mailer)
	result, err := userService.GetVerified()
	if err != nil {
		return c.Status(500).JSON(&fiber.Map{
			"success": false,
			"message": err.Error(),
			"data":    nil,
		})
	}

	return c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "successfully call verified user",
		"data":    result,
	})
}
