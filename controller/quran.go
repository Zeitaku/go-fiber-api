package controller

import (
	"go-api/service"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

type QuranController struct {
	Cli *http.Client
}

func NewQuranController(cli *http.Client) QuranController {
	return QuranController{Cli: cli}
}
func (qs QuranController) GetAllSurat(c *fiber.Ctx) error {
	QuranService := service.NewQuranService(qs.Cli)
	result, err := QuranService.GetAllSurat()
	if err != nil {
		return c.Status(500).JSON(&fiber.Map{
			"success": false,
			"message": "Internal Server Error",
			"data":    nil,
		})
	}

	return c.Status(200).JSON(&fiber.Map{
		"success": true,
		"message": "",
		"data":    result,
	})
}
