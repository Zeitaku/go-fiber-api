package repository

import (
	"errors"
	"fmt"
	"go-api/model"
	"math"
	"strconv"

	"gorm.io/gorm"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return UserRepository{
		db: db,
	}
}

func (ur UserRepository) GetHashedPassword(id string) (string, error) {
	var user model.User
	result := ur.db.First(&user, id)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return "", errors.New("user not found")
		}
		return "", result.Error
	}
	return user.Password, nil
}
func (ur UserRepository) GetAllUsers(param model.PaginationParam) (model.Pagination, []model.User, error) {
	var users []model.User
	var allUser []model.User
	var PaginationResult model.Pagination
	result := ur.db.Limit(param.Limit).Offset(param.Offset).Find(&users)
	if result.Error != nil {
		return model.Pagination{}, nil, result.Error
	}
	allUserRes := ur.db.Find(&allUser)
	if allUserRes.Error != nil {
		return model.Pagination{}, nil, allUserRes.Error
	}
	PaginationResult.CurrentPage = param.Page
	lastPage := float64(len(allUser)) / float64(param.Limit)
	PaginationResult.LastPage = int(math.Ceil(lastPage))
	PaginationResult.PageLimit = param.Limit
	PaginationResult.TotalData = len(allUser)
	if PaginationResult.LastPage < param.Page {
		return PaginationResult, nil, errors.New(fmt.Sprintf("Page melebihi batas, LastPage = %d", PaginationResult.LastPage))
	}
	return PaginationResult, users, nil
}
func (ur UserRepository) GetUser(id string) (model.User, error) {
	var user model.User
	result := ur.db.First(&user, id)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return user, errors.New("user not found")
		}
		return user, result.Error
	}
	return user, nil
}

func (ur UserRepository) CreateUser(name string, email string, password string) (model.User, error) {
	newUser := model.User{Name: name, Email: email, Password: password}
	var result model.User
	checkEmail := ur.db.First(&result, "Email = ?", email)
	if checkEmail.RowsAffected > 0 {
		return model.User{}, errors.New("email telah digunakan")
	} else if errors.Is(checkEmail.Error, gorm.ErrRecordNotFound) {
		ur.db.Create(&newUser)
		ur.db.Last(&result)
		return result, nil
	} else {
		return model.User{}, checkEmail.Error
	}
}

func (ur UserRepository) UpdateUser(name string, id string) (model.User, error) {
	var user model.User
	checkName := ur.db.First(&user, "Name = ?", name)
	var resultID string = strconv.FormatUint(uint64(user.ID), 10)
	if checkName.RowsAffected > 0 {
		if user.Name == name && resultID != id {
			return model.User{}, errors.New("nama telah digunakan")
		}
	}
	ur.db.Model(&user).Where("ID = ?", id).Updates(model.User{Name: name})
	result, err := ur.GetUser(id)
	if err != nil {
		return result, err
	}
	return result, nil
}

func (ur UserRepository) DeleteUser(id string) (model.User, error) {
	result, err := ur.GetUser(id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return result, errors.New("user not found")
		}
		return result, err
	}
	ur.db.Delete(&model.User{}, id)
	return result, nil
}

func (ur UserRepository) Login(email string) (model.User, error) {
	var user model.User
	result := ur.db.First(&user, "Email = ?", email)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return user, errors.New("email not found")
		}
		return user, result.Error
	}
	if !user.IsVerified {
		return user, errors.New("email is not verified")
	}
	return user, nil
}

func (ur UserRepository) UserProfile(id string) (model.User, error) {
	var user model.User
	result := ur.db.First(&user, "id = ?", id)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return user, errors.New("user not found")
		}
		return user, result.Error
	}
	return user, nil
}

func (ur UserRepository) UpdateUserPassword(newPassword, id string) (model.User, error) {
	var user model.User
	ur.db.Model(&user).Where("ID = ?", id).Updates(model.User{Password: newPassword})
	result, err := ur.GetUser(id)
	if err != nil {
		return result, err
	}
	return result, nil
}
func (ur UserRepository) GetUserByEmail(email string) (model.User, error) {
	var user model.User
	result := ur.db.First(&user, "Email = ?", email)
	if result.Error == gorm.ErrRecordNotFound {
		return user, errors.New("email not found")
	}
	if result.Error != nil {
		return user, result.Error
	}
	return user, nil
}

func (ur UserRepository) SetVerify(email string) (model.User, error) {
	var user model.User
	// add vrify check
	update := ur.db.Model(&user).Where("Email = ?", email).Updates(model.User{IsVerified: true})
	if update.Error != nil {
		return user, update.Error
	}
	userDetail, err := ur.GetUserByEmail(email)
	if err != nil {
		return user, err
	}
	return userDetail, nil
}
func (ur UserRepository) GetVerified() ([]model.User, error) {
	var users []model.User
	tx := ur.db.Find(&users, "is_verified = ?", true)
	if tx.Error != nil {
		return users, tx.Error
	}
	return users, nil
}
