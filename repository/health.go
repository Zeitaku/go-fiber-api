package repository

import "gorm.io/gorm"

type HealthRepository struct {
	db *gorm.DB
}

func NewHealthRepository(db *gorm.DB) HealthRepository {
	return HealthRepository{
		db: db,
	}
}

func (hr HealthRepository) CheckHealth(db *gorm.DB) (string, error) {
	sqlDB, err := hr.db.DB()
	if err != nil {
		return "Not OK", err
	}
	err = sqlDB.Ping()
	if err != nil {
		return "Not OK", err
	}
	return "OK", err
}
