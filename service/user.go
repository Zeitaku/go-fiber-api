package service

import (
	"errors"
	"fmt"
	"go-api/config"
	"go-api/model"
	"go-api/repository"
	"os"
	"strconv"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/gomail.v2"
	"gorm.io/gorm"
)

type UserService struct {
	db     *gorm.DB
	Mailer *gomail.Dialer
}

func NewUserService(db *gorm.DB, mailer *gomail.Dialer) UserService {
	return UserService{db: db, Mailer: mailer}
}
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}
func CheckPasswordHash(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
func (us UserService) GetHashedPassword(id string) (string, error) {
	userRepository := repository.NewUserRepository(us.db)
	pass, err := userRepository.GetHashedPassword(id)
	if err != nil {
		return "", err
	}
	return pass, nil
}

func (us UserService) GetAllUsers(page string, limit string) (model.Pagination, []model.ResponseUser, error) {
	userRepository := repository.NewUserRepository(us.db)
	intPage, err := strconv.Atoi(page)
	if err != nil {
		return model.Pagination{}, nil, err
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil {
		return model.Pagination{}, nil, err
	}
	var paginationParam model.PaginationParam
	paginationParam.Limit = intLimit
	paginationParam.Page = intPage
	paginationParam.Offset = (intPage - 1) * intLimit
	pagination, users, err := userRepository.GetAllUsers(paginationParam)
	if err != nil {
		return model.Pagination{}, nil, err
	}
	var responseUsers []model.ResponseUser
	for _, user := range users {
		responseUser := model.ResponseUser{
			ID:         uint64(user.ID),
			Email:      user.Email,
			Name:       user.Name,
			IsVerified: user.IsVerified,
		}
		responseUsers = append(responseUsers, responseUser)
	}
	return pagination, responseUsers, nil
}

func (us UserService) GetUser(id string) (model.ResponseUser, error) {
	userRepository := repository.NewUserRepository(us.db)
	user, err := userRepository.GetUser(id)
	responseUser := model.ResponseUser{
		ID:    uint64(user.ID),
		Email: user.Email,
		Name:  user.Name,
	}
	if err != nil {
		return model.ResponseUser{}, err
	}
	return responseUser, nil
}

func (us UserService) CreateUser(name string, email string, password string) (model.ResponseUser, error) {
	userRepository := repository.NewUserRepository(us.db)
	hashedPass, err := HashPassword(password)
	if err != nil {
		return model.ResponseUser{}, err
	}
	user, err := userRepository.CreateUser(name, email, hashedPass)
	responseUser := model.ResponseUser{
		ID:    uint64(user.ID),
		Email: user.Email,
		Name:  user.Name,
	}
	if err != nil {
		return model.ResponseUser{}, err
	}
	emailLink := fmt.Sprintf("<a href='%s'>%s</a>", "http://localhost:8080/api/verifyemail?email="+email, "Verification Link")
	err = config.SendMessage(us.Mailer, os.Getenv("SMTP_SENDER"), []string{email}, os.Getenv("SMTP_SENDER"), "Go API test", "test mail", emailLink)
	if err != nil {
		return model.ResponseUser{}, err
	}

	return responseUser, nil
}

func (us UserService) UpdateUser(name string, id string) (model.ResponseUser, error) {
	userRepository := repository.NewUserRepository(us.db)
	user, err := userRepository.UpdateUser(name, id)
	responseUser := model.ResponseUser{
		ID:    uint64(user.ID),
		Email: user.Email,
		Name:  user.Name,
	}
	if err != nil {
		return model.ResponseUser{}, err
	}
	return responseUser, nil
}

func (us UserService) DeleteUser(id string) (model.ResponseUser, error) {
	userRepository := repository.NewUserRepository(us.db)
	delete, err := userRepository.DeleteUser(id)
	if err != nil {
		return model.ResponseUser{}, err
	}
	responseUser := model.ResponseUser{
		ID:    uint64(delete.ID),
		Email: delete.Email,
		Name:  delete.Name,
	}
	return responseUser, nil
}

func (us UserService) Login(email string, password string) (model.LoginUser, error) {
	userRepository := repository.NewUserRepository(us.db)
	result, err := userRepository.Login(email)
	var loginUser model.LoginUser
	loginUser.Name = result.Name
	loginUser.Email = result.Email
	loginUser.Password = result.Password
	if err != nil {
		return model.LoginUser{}, err
	}
	if !CheckPasswordHash(password, result.Password) {
		return model.LoginUser{}, errors.New("wrong password")
	}
	//create data yang akan di encode jika user ditemukan dalam database, token berlaku selama 3 hari/72 jam
	claims := jwt.MapClaims{
		"id":    result.ID,
		"email": result.Email,
		"admin": true,
		"exp":   time.Now().Add(time.Hour * 72).Unix(),
	}
	//sign json claims, dengan metode hs256
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	//sign token dari userService.Login dengan signature "secret"
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return loginUser, err
	}
	loginUser.Token = t
	return loginUser, nil
}

func (us UserService) UserProfile(id string) (model.ResponseUser, error) {
	userRepository := repository.NewUserRepository(us.db)
	result, err := userRepository.UserProfile(id)
	if err != nil {
		return model.ResponseUser{}, err
	}
	var responseUser model.ResponseUser
	responseUser.ID = uint64(result.ID)
	responseUser.Email = result.Email
	responseUser.Name = result.Name
	return responseUser, nil
}

func (us UserService) UpdateUserPassword(oldPassword, newPassword, id string) (model.ResponseUser, error) {
	userRepository := repository.NewUserRepository(us.db)
	hashedOldPass, err := us.GetHashedPassword(id)
	if err != nil {
		return model.ResponseUser{}, err
	}
	if CheckPasswordHash(oldPassword, hashedOldPass) {
		hashedNewPassword, err := HashPassword(newPassword)
		if err != nil {
			return model.ResponseUser{}, err
		}
		user, err := userRepository.UpdateUserPassword(hashedNewPassword, id)
		if err != nil {
			return model.ResponseUser{}, err
		}
		responseUser := model.ResponseUser{
			ID:    uint64(user.ID),
			Email: user.Email,
			Name:  user.Name,
		}
		return responseUser, nil
	}
	return model.ResponseUser{}, errors.New("wrong password")
}
func (us UserService) VerifyEmail(email string) (model.ResponseUser, error) {
	userRepository := repository.NewUserRepository(us.db)
	_, err := userRepository.GetUserByEmail(email)
	if err != nil {
		return model.ResponseUser{}, err
	}
	user, err := userRepository.SetVerify(email)
	if err != nil {
		return model.ResponseUser{}, err
	}
	var result model.ResponseUser
	result.ID = uint64(user.ID)
	result.Name = user.Name
	result.Email = user.Email
	return result, nil
}
func (us UserService) GetVerified() ([]model.ResponseUser, error) {
	userRepository := repository.NewUserRepository(us.db)
	users, err := userRepository.GetVerified()
	var responseUsers []model.ResponseUser
	if err != nil {
		return responseUsers, err
	}
	for _, user := range users {
		responseUser := model.ResponseUser{
			ID:    uint64(user.ID),
			Email: user.Email,
			Name:  user.Name,
		}
		responseUsers = append(responseUsers, responseUser)
	}
	return responseUsers, nil
}
