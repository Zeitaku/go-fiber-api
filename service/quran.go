package service

import (
	"go-api/model"
	"go-api/requester"
	"net/http"
)

type QuranService struct {
	Cli *http.Client
}

func NewQuranService(cli *http.Client) QuranService {
	return QuranService{Cli: cli}
}

func (qs QuranService) GetAllSurat() ([]model.QuranResponse, error) {
	quranRequester := requester.NewQuranRequester(qs.Cli)
	res, err := quranRequester.GetAllSurat()
	if err != nil {
		return res, err
	}
	return res, nil
}
