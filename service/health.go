package service

import (
	"go-api/repository"

	"gorm.io/gorm"
)

type HealthService struct {
	db *gorm.DB
}

func NewHealthService(db *gorm.DB) HealthService {
	return HealthService{db: db}
}

func (hs HealthService) CheckHealth() (string, error) {
	healthRepository := repository.NewHealthRepository(hs.db)
	res, err := healthRepository.CheckHealth(hs.db)
	if err != nil {
		return res, err
	}
	return res, nil
}
