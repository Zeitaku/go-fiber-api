package model

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Name       string
	Email      string
	Password   string
	IsVerified bool
}
type ResponseUser struct {
	ID         uint64 `json:"id"`
	Name       string `json:"name"`
	Email      string `json:"email"`
	IsVerified bool   `json:"is_verified,omitempty"`
}
type LoginUser struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"-"`
	Token    string `json:"token"`
}
type UpdatePass struct {
	OldPassword string `json:"oldpassword"`
	NewPassword string `json:"newpassword"`
}
type ChangeEmail struct {
	Password string `json:"password"`
	Email    string `json:"email"`
}
type Pagination struct {
	CurrentPage int `json:"CurrentPage"`
	LastPage    int `json:"LastPage"`
	PageLimit   int `json:"PageLimit"`
	TotalData   int `json:"TotalData"`
}

type PaginationParam struct {
	Limit  int
	Page   int
	Offset int
}
