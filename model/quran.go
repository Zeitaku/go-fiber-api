package model

type QuranResponse struct {
	Arti       string `json:"arti"`
	Asma       string `json:"asma"`
	Ayat       int    `json:"ayat"`
	Nama       string `json:"nama"`
	Type       string `json:"type"`
	Urut       string `json:"urut"`
	Audio      string `json:"audio"`
	Nomor      string `json:"nomor"`
	Rukuk      string `json:"rukuk"`
	Keterangan string `json:"keterangan"`
}

const QuranUrl string = "https://api.npoint.io/99c279bb173a6e28359c/data"
