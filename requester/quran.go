package requester

import (
	"encoding/json"
	"go-api/model"
	"net/http"
)

type QuranRequester struct {
	Cli *http.Client
}

func NewQuranRequester(cli *http.Client) QuranRequester {
	return QuranRequester{Cli: cli}
}

func (qr QuranRequester) GetAllSurat() ([]model.QuranResponse, error) {
	var err error
	var data []model.QuranResponse

	request, err := http.NewRequest("GET", model.QuranUrl, nil)
	if err != nil {
		return nil, err
	}

	response, err := qr.Cli.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&data)
	if err != nil {
		return nil, err
	}

	return data, nil
}
