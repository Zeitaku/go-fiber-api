package main

import (
	"go-api/config"
	"go-api/controller"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	_ "github.com/gofiber/fiber/v2/middleware/logger"
	jwtware "github.com/gofiber/jwt/v3"
	_ "github.com/joho/godotenv/autoload"
	"github.com/sirupsen/logrus"
	"gopkg.in/gomail.v2"
	"gorm.io/gorm"
)

func status(c *fiber.Ctx) error {
	return c.SendString("Server is running! Send your request")
}

func setupRoutes(app *fiber.App, db *gorm.DB, logger *logrus.Logger, client *http.Client, mailer *gomail.Dialer) {
	userController := controller.NewUserController(db, logger, mailer)
	healthController := controller.NewHealthController(db)
	quranController := controller.NewQuranController(client)
	kodePosController := controller.NewKodePosController(client)

	app.Get("/", status)
	app.Get("/api/health", healthController.HealthCheck)

	app.Get("/api/user", userController.GetAllUsers)
	app.Post("/api/user", userController.SaveUser)
	app.Get("/api/user/:id", userController.GetUser)
	app.Put("/api/user/:id", userController.UpdateUser)
	app.Delete("/api/user/:id", userController.DeleteUser)
	app.Put("api/userpass/:id", userController.UpdateUserPassword)
	app.Post("/api/user/login", userController.Login)
	app.Get("api/verifyemail", userController.VerifyEmail)
	app.Get("api/verified", userController.GetVerified)

	//external api requester
	app.Get("/api/quran", quranController.GetAllSurat)
	app.Get("/api/kodepos", kodePosController.GetAllKodePos)

	//verify jwt token, dengan signature "secret"
	app.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte("secret"),
	}))
	app.Get("/api/profile", userController.UserProfile)
}

func main() {
	app := fiber.New()
	// app.Use(logger.New())
	db, dbErr := config.InitDatabase()
	if dbErr != nil {
		panic(dbErr)
	}

	//smtp config
	dialer, err := config.InitGomail()
	if err != nil {
		panic(err)
	}

	// Default config
	app.Use(cors.New())
	//initiate logger
	log := logrus.New()
	client := &http.Client{}
	setupRoutes(app, db, log, client, dialer)
	//memanggil func restricted
	app.Listen(":8080")
}
